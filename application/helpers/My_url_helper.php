<?php

function buildParams($params = array()) {
    if (!count($params)) {
        return '';
    }

    $_params = array();
    foreach ($params as $key => $value) {
        array_push($_params, "$key=$value");
    }

    return '?' . join($_params, '&');
}

/*************** page uri ***************/
function getDashboardUri($params = array()) {
    return '/' . buildParams($params);
}

function getViewPDFUri($params = array()) {
    return '/view-pdf' . buildParams($params);
}

function getDownloadPDFUri($params = array()) {
    $params['download'] = true;
    return getViewPDFUri($params);
}

function getLoginUri($params = array()) {
    return '/login' . buildParams($params);
}

function getUserAccountUri($params = array()) {
    return '/account' . buildParams($params);
}

function getViewUserUri($params = array()) {
    return '/user' . buildParams($params);
}

function getEditAccountUri($params = array()) {
    return '/edit-account' . buildParams($params);
}

function getUploadPdfUri($params = array()) {
    return '/upload' . buildParams($params);
}

function getChangePasswordUri($params = array()) {
    return '/change-password' . buildParams($params);
}

function getSignOutUri($params = array()) {
    return '/signout' . buildParams($params);
}

function getSignUpUri($params = array()) {
    return '/signup' . buildParams($params);
}

function getAddCommentUri($params = array()) {
    return '/add-comment' . buildParams($params);
}

function getPageNotFoundUri($params = array()) {
    return '/page-not-found' . buildParams($params);
}

/*************** js path ***************/
function getPDFJS() {
    return './public/js/pdfjs-1.0.1040-dist/build/pdf.js';
}

function getPDFWorkerJS() {
    return './public/js/pdfjs-1.0.1040-dist/build/pdf.worker.js';
}

function getJQueryJS() {
    return './public/js/jquery-1.11.2.min.js';
}

/*************** uploads path ***************/
function getPDFPath($user_id) {
    return "./uploads/$user_id";
}

function getPDFFile($user_id, $name) {
    return getPDFPath($user_id) . "/$name";
}

/*************** css path ***************/
function getCommonCss() {
    return "./public/css/common.css";
}
