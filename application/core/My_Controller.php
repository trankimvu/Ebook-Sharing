<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    protected function ensureInputGet($key, $default = null) {
        $value = $this->input->get($key);

        if ($value) {
            return $value;
        }

        if ($default !== null) {
            return $default;
        }

        $this->toPageNotFound();
    }

    protected function ensureInputPost($key, $default = null) {
        $value = $this->input->post($key);

        if ($value) {
            return $value;
        }

        if ($default !== null) {
            return $default;
        }

        $this->toPageNotFound();
    }

    protected function toPageNotFound() {
        redirect(getPageNotFoundUri());
    }
    
    protected function showView($view, $data = array()) {
        $this->load->view('header');
        $this->load->view($view, $data);
        $this->load->view('footer');
    }

}
