<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class My_Model extends CI_Model {

    protected $_name = 'table name';

    public function __construct() {
        parent::__construct();
    }

    public function getById($id, $column_name = 'id') {
        $this->db->where(
                array(
                    $column_name => $id
                )
        );

        $query = $this->db->get($this->_name);
        return $query->row();
    }

    public function getAll() {
        $query = $this->db->get($this->_name);
        return $query->result_array();
    }
}
