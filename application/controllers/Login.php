<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends My_Controller {

    public function index() {
        $data = array();
        do {
            if ($this->input->server('REQUEST_METHOD') !== 'POST') {
                break;
            }

            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $this->load->model('model_user');
            $user = $this->model_user->auth($username, $password);
            if (!$user) {
                $data['username'] = $username;
                $data['message'] = 'Username or password not correct';
                break;
            }

            $session_data = array(
                'id' => $user->id,
                'username' => $username
            );
            $this->session->set_userdata($session_data);

            $this->load->helper('url');
            redirect(getDashboardUri());
        } while (false);

        $this->showView('login', $data);
    }

    public function signup() {
        $this->load->model('model_user');
        $data = array(
            'username' => '',
            'firstname' => '',
            'lastname' => '',
            'genders' => $this->model_user->getGenders(),
            'gender' => $this->model_user->getDefaultGender(),
            'email' => ''
        );

        do {
            if ($this->input->server('REQUEST_METHOD') !== 'POST') {
                break;
            }

            $data['username'] = $this->input->post('username');
            $data['firstname'] = $this->input->post('firstname');
            $data['lastname'] = $this->input->post('lastname');
            $data['gender'] = $this->input->post('gender');
            $data['email'] = $this->input->post('email');

            $password = $this->input->post('password');
            $retype_password = $this->input->post('retype_password');

            if (!$data['username']) {
                $data['message'] = 'Please select your username';
                break;
            }

            if (!$password) {
                $data['message'] = 'Please add your password';
                break;
            }

            if ($password !== $retype_password) {
                $data['message'] = 'Password does not match';
                break;
            }

            if ($this->model_user->isUsernameInUsed($data['username'])) {
                $data['message'] = 'Username has been use';
                break;
            }

            $this->model_user->addUser(
                    $data['username'], $password, $data['email'], $data['firstname'], $data['lastname'], $data['gender']
            );

            $this->load->helper('url');
            redirect(getLoginUri());
        } while (false);

        $this->load->helper('form');
        $this->showView('signup', $data);
    }

    public function signout() {
        $this->session->sess_destroy();

        $this->load->helper('url');
        redirect(getLoginUri());
    }

}
