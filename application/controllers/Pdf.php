<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Pdf extends My_Controller {

    public function view() {
        $file_id = $this->ensureInputGet('id');
        $download = $this->input->get('download');

        $this->load->model('model_pdf');
        $file = $this->model_pdf->getById($file_id);
        if (!$file)
        {
            $this->toPageNotFound();
        }
        $path = getPDFFile($file->username, $file->file_name);

        if ($download) {
            $this->load->helper('download');
            $name = $file->file_name;
            $data = file_get_contents($path);
            force_download($name, $data);
            return;
        }

        $this->load->model('model_comment');
        $page = $this->input->get('page');
        if (!$page) {
            $page = 1;
        }
        $comments = $this->model_comment->getComment($file_id, $page);

        $data = array(
            'id' => $file_id,
            'pdf_path' => $path,
            'comments' => $comments
        );

        $this->load->helper('form');
        $this->showView('view-pdf', $data);
    }

    public function upload() {
        $data = array();
        do {
            if ($this->input->server('REQUEST_METHOD') !== 'POST') {
                break;
            }

            $upload_path = getPDFPath($this->session->userdata('username')) . '/';
            if (!is_dir($upload_path)) {
                mkdir($upload_path, 0777);
            }
            $config['upload_path'] = $upload_path;
            $config['allowed_types'] = 'pdf';
            $config['max_size'] = '1000000';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload()) {
                $data['message'] = $this->upload->display_errors();
                break;
            }

            $category = $this->input->post('category');
            $display_name = $this->input->post('display_name');
            $file_upload = $this->upload->data();
            $this->load->model('model_pdf');
            $this->model_pdf->addFile(
                    $this->session->userdata('username'), $file_upload['file_name'], $display_name, $category
            );
            $data['message'] = $file_upload['file_name'] . ' upload success.';
        } while (false);

        $this->load->model('model_category');
        $categories = $this->model_category->getAll();
        $data['categories'] = array();
        for ($i = 0; $i < count($categories); $i++) {
            $data['categories'][$categories[$i]['id']] = $categories[$i]['name'];
        }

        $this->load->helper('form');
        $this->showView('upload', $data);
    }

    public function comment() {
        do {
            if ($this->input->server('REQUEST_METHOD') !== 'POST') {
                break;
            }

            $username = $this->session->userdata('username');
            $file_id = $this->input->post('file_id');
            $content = $this->input->post('content');
            $this->load->model('model_comment');
            $this->model_comment->addComment($username, $file_id, $content);
        } while (false);

        redirect(getViewPDFUri(array('id' => $file_id)));
    }

}
