<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends My_Controller {

    public function view() {
        $username = $this->ensureInputGet('username');
        $this->load->model('model_user');
        $user = $this->model_user->getById($username, 'username');
        if (!$user) {
            $this->toPageNotFound();
        }

        $data = array(
            'user' => $user
        );

        $this->showView('view-user', $data);
    }

    public function account() {
        $user_data = $this->session->all_userdata();
        $this->load->model('model_user');
        $user = $this->model_user->getById($user_data['id']);

        if (!$user) {
            $this->load->helper('url');
            redirect(getLoginUri());
        }

        $data = array(
            'user' => $user
        );

        $this->showView('account', $data);
    }

    public function edit() {
        $user_data = $this->session->all_userdata();
        $this->load->model('model_user');
        $user = $this->model_user->getById($user_data['id']);

        if (!$user) {
            $this->load->helper('url');
            redirect(getLoginUri());
        }

        do {
            if ($this->input->server('REQUEST_METHOD') !== 'POST') {
                break;
            }

            $email = $this->input->post('email');
            $firstname = $this->input->post('firstname');
            $lastname = $this->input->post('lastname');
            $gender = $this->input->post('gender');
            $this->model_user->updateUser($user_data['id'], $email, $firstname, $lastname, $gender);
            $this->load->helper('url');
            redirect(getUserAccountUri());
        } while (false);

        $this->load->model('model_user');
        $data = array(
            'user' => $user,
            'genders' => $this->model_user->getGenders()
        );
        $this->load->helper('form');
        $this->showView('edit-account', $data);
    }

    public function password() {
        $user_data = $this->session->all_userdata();
        $this->load->model('model_user');
        $user = $this->model_user->getById($user_data['id']);

        if (!$user) {
            $this->load->helper('url');
            redirect(getLoginUri());
        }

        do {
            if ($this->input->server('REQUEST_METHOD') !== 'POST') {
                break;
            }

            $password = $this->input->post('password');
            $retype_password = $this->input->post('retype_password');

            if ($password !== $retype_password) {
                $data['message'] = 'Password Does Not Match';
                break;
            }

            $this->model_user->updatePassword($user_data['id'], $password);
            $this->load->helper('url');
            redirect(getUserAccountUri());
        } while (false);

        $data = array(
            'user' => $user
        );
        $this->showView('change-password', $data);
    }

}
