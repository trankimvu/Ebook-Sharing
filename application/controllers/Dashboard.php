<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends My_Controller {

    public function index() {
        $filter = $this->input->get('filter');
        $category = $this->input->get('category');
        $page = $this->input->get('page');
        if (!$page) {
            $page = 1;
        }

        $this->load->model('model_pdf');
        $files = $this->model_pdf->getFiles($filter, $category, $page);

        $this->load->model('model_category');
        $categories = $this->model_category->getAll();
        $_categories = array('0' => 'Select Category');
        for ($i = 0; $i < count($categories); $i++) {
            $_categories[$categories[$i]['id']] = $categories[$i]['name'];
        }

        $data = array(
            'filter' => $filter,
            'categories' => $_categories,
            'category' => $category,
            'page' => $page,
            'files' => $files
        );

        $this->load->helper('form');
        $this->showView('dashboard', $data);
    }

    public function pagenotfound() {
        $this->showView('page-not-found');
    }

}
