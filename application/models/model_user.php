<?php

class Model_user extends My_Model {

    protected $_name = 'user';
    protected $_DEFAULT_GENDER = 'unknown';
    protected $_GENDERS = array(
        'unknown' => 'Gender',
        'male' => 'male',
        'female' => 'Female'
    );

    public function auth($username, $password) {
        $this->db->where(
            array(
                'username' => $username,
                'password' => $password
            )
        );

        $query = $this->db->get($this->_name);
        return $query->row();
    }

    public function isUsernameInUsed($username) {
        $this->db->where(
            array(
                'username' => $username
            )
        );

        $query = $this->db->get($this->_name);
        return $query->row();
    }

    public function addUser($username, $password, $email, $firstname, $lastname, $gender) {
        return $this->db->insert(
            $this->_name,
            array(
                'username' => $username,
                'password' => $password,
                'email' => $email,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'gender' => $gender
            )
        );
    }

    public function updateUser($id, $email, $firstname, $lastname, $gender) {
        $this->db->where('id', $id);
        return $this->db->update(
            $this->_name,
            array(
                'email' => $email,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'gender' => $gender
            )
        );
    }

    public function updatePassword($id, $password) {
        $this->db->where('id', $id);
        return $this->db->update(
            $this->_name,
            array(
                'password' => $password
            )
        );
    }

    public function getGenders() {
        return $this->_GENDERS;
    }

    public function getDefaultGender() {
        return $this->_DEFAULT_GENDER;
    }

}
