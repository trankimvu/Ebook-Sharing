<?php

class Model_comment extends My_Model {

    protected $_name = 'comment';
    protected $_limit = 50;

    public function getComment($file_id, $page) {
        $this->db->where(
                array(
                    'file_id' => $file_id
                )
        );

        $query = $this->db->get($this->_name, $this->_limit * ($page - 1), $this->_limit);
        return $query->result_array();
    }
    
    public function addComment($username, $file_id, $content) {
        return $this->db->insert(
            $this->_name,
            array(
                'username' => $username,
                'file_id' => $file_id,
                'content' => $content
            )
        );
    }

}
