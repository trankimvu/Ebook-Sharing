<?php

class Model_pdf extends My_Model {

    protected $_name = 'pdf';
    protected $_limit = 50;

    public function getFiles($filter, $category, $page) {
        if ($filter)
        {
            $this->db->where("(display_name LIKE '%$filter%' OR file_name LIKE '%$filter%')");
        }

        if ($category)
        {
            $this->db->where('category_id', $category);
        }

        $this->db->order_by("created_time", "desc");
        $query = $this->db->get($this->_name, $this->_limit * ($page - 1), $this->_limit);
        return $query->result_array();
    }

    public function addFile($username, $file_name, $display_name, $category) {
        return $this->db->insert(
            $this->_name,
            array(
                'username' => $username,
                'file_name' => $file_name,
                'display_name' => $display_name,
                'category_id' => $category
            )
        );
    }

}
