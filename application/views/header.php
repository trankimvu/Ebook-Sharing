<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
    <link rel="stylesheet" type="text/css" href="<?php echo(getCommonCss()); ?>">
    This is header
    <a href="/">Home</a>
    <?php if ($this->session->userdata('id')): ?>
    <a href="<?php echo(getUserAccountUri()); ?>">Account</a>
    <a href="<?php echo(getSignOutUri()); ?>">Sign Out</a>
    <?php else: ?>
    <a href="<?php echo(getLoginUri()); ?>">Login</a>
    <?php endif; ?>
    <br/>
    -------------------------------------------------------------------------------
</div>