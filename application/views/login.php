<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<form method="POST" action="<?php echo(getLoginUri()); ?>">
    <input type="text" name="username" placeholder="Username"
    <?php
    if (isset($username)) {
        echo("value='$username'");
    }
    ?>>
    <input type="password" name="password" placeholder="Password">
    <?php
    if (isset($message)) {
        echo($message);
    }
    ?>
    <input type="submit" value="Login">
</form>
<a href="<?php echo(getSignUpUri()); ?>">Sign Up</a>