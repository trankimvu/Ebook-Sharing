<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<a href="<?php echo(getDownloadPDFUri(array('id' => $id))); ?>">Download</a>
<script src="<?php echo(getPDFJS()); ?>"></script>
<script src="<?php echo(getJQueryJS()); ?>"></script>
<script>
    PDFJS.workerSrc = '<?php echo(getPDFWorkerJS()); ?>';
    function loadPage(pdf, pageNumber) {
        pdf.getPage(pageNumber).then(function(page) {
            var scale = 1.5;
            var viewport = page.getViewport(scale);

            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            var renderContext = {
                canvasContext: context,
                viewport: viewport
            };
            page.render(renderContext);

            var pageDecorator = $('<div>')
                    .attr({style: 'border:1px solid black'}).append(canvas);
            $('#pdf-content').append(pageDecorator);
            setTimeout(loadPage.bind(null, pdf, pageNumber + 1), 500);
        });
    }
    PDFJS.getDocument('<?php echo($pdf_path); ?>').then(function(pdf) {
        loadPage(pdf, 1);
    });
</script>
<div id="pdf-content"></div>
<?php for ($i = 0; $i < count($comments); $i++): ?>
<div>
        <?php echo($comments[$i]['username'] . ': ' . $comments[$i]['content']); ?>
</div>
<?php endfor; ?>
<?php
echo form_open(getAddCommentUri(), array('method' => 'POST'));
echo form_textarea(array('name' => 'content'));
echo form_hidden('file_id', $id);
echo form_submit(false, 'Add');
echo form_close();