<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
    Username: <?php echo($user->username); ?>
    <br/>
    First Name: <?php echo($user->firstname); ?>
    <br/>
    Last Name: <?php echo($user->lastname); ?>
    <br/>
    Email: <?php echo($user->email); ?>
    <br/>
    Gender: <?php echo($user->gender); ?>
    <br/>
    <a href="<?php echo(getEditAccountUri()); ?>">Edit</a>
    <a href="<?php echo(getChangePasswordUri()); ?>">Change Password</a>
    <a href="<?php echo(getUploadPdfUri()); ?>">Upload File</a>
</div>