<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
echo form_open(getDashboardUri(), array('method' => 'GET'));
echo form_input(array('type' => 'text', 'name' => 'filter', 'value' => $filter));
echo form_dropdown('category', $categories, array('value' => $category));
echo form_submit(false, 'Search');
echo form_close();
?>
<?php foreach ($files as $file): ?>
    <a href="<?php echo(getViewPDFUri(array('id' => $file['id']))); ?>" target="_blank">
        <?php echo($file['display_name'] ? $file['display_name'] : $file['file_name']); ?>
    </a>
    by <a href='<?php echo(getViewUserUri(array('username' => $file['username']))); ?>'><?php echo($file['username']); ?></a>
    at <?php echo($file['created_time']); ?>
    <br/>
<?php endforeach;