<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
if (isset($message)) {
    echo($message);
}
?>
<?php
echo form_open(getSignUpUri(), array('method' => 'POST'));
echo form_input(array('name' => 'username', 'placeholder' => 'Username', 'value' => $username));
echo form_password(array('name' => 'password', 'placeholder' => 'Password'));
echo form_password(array('name' => 'retype_password', 'placeholder' => 'Retype Password'));
echo form_input(array('name' => 'firstname', 'placeholder' => 'Firstname', 'value' => $firstname));
echo form_input(array('name' => 'lastname', 'placeholder' => 'Lastname', 'value' => $lastname));
echo form_input(array('name' => 'email', 'placeholder' => 'Email', 'value' => $email));
echo form_dropdown('gender', $genders, array('value' => $gender));
echo form_submit(false, 'Sign Up');
echo form_close();