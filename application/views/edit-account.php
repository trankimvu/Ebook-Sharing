<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php
echo form_open(getEditAccountUri(), array('method' => 'POST'));
echo form_input(array('name' => 'firstname', 'placeholder' => 'Firstname', 'value' => $user->firstname));
echo form_input(array('name' => 'lastname', 'placeholder' => 'Lastname', 'value' => $user->lastname));
echo form_input(array('name' => 'email', 'placeholder' => 'Email', 'value' => $user->email));
echo form_dropdown('gender', $genders, array('value' => $user->gender));
echo form_submit(false, 'Save');
echo form_close();