<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<div>
    <?php
    if (isset($message)) {
        echo($message);
    }

    echo form_open_multipart(getUploadPdfUri());
    echo form_input(array('type' => 'file', 'name' => 'userfile'));
    echo form_input(array('type' => 'text', 'name' => 'display_name', 'placeholder' => 'File Name'));
    echo form_dropdown('category', $categories);
    echo form_submit('submit', 'upload');
    echo form_close();
    ?>
</div>