<?php

class Session {

    protected $exception = array(
        'login/index',
        'login/signup',
        'login/signout',
    );

    public function __construct() {
        $this->CI = &get_instance();
    }

    public function checkUserSession() {
        if ($this->isExceptionPage()) {
            return;
        }

        $user_data = $this->CI->session->all_userdata();
        if (!$user_data['id'])
        {
            $this->CI->load->helper('url');
            redirect('/login');
        }
    }

    protected function isExceptionPage() {
        $class = $this->CI->router->fetch_class();
        $method = $this->CI->router->fetch_method();

        if (array_search("$class/$method", $this->exception) === false) {
            return false;
        }

        return true;
    }

}
