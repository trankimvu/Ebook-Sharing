/*
SQLyog Ultimate v11.11 (32 bit)
MySQL - 5.6.14 : Database - ebook_sharing
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `category` */

LOCK TABLES `category` WRITE;

insert  into `category`(`id`,`name`,`created_time`) values (1,'Comic','2015-04-07 00:59:36'),(2,'Adventure','2015-04-07 00:59:36'),(3,'Funny','2015-04-07 00:59:36'),(4,'Mystery','2015-04-07 00:59:36'),(5,'Ghost','2015-04-07 00:59:36');

UNLOCK TABLES;

/*Table structure for table `comment` */

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `file_id` int(10) unsigned NOT NULL,
  `content` text NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_comment_user` (`username`),
  CONSTRAINT `FK_comment_user` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `comment` */

LOCK TABLES `comment` WRITE;

insert  into `comment`(`id`,`username`,`file_id`,`content`,`created_time`) values (1,'test1',2,'hello','2015-04-07 00:58:42'),(2,'test1',1,'test','2015-04-07 01:06:21'),(3,'test1',1,'test 1','2015-04-07 01:07:19'),(4,'test1',1,'asd\r\n','2015-04-07 01:34:52'),(5,'test123456',3,'test','2015-04-09 21:41:57');

UNLOCK TABLES;

/*Table structure for table `pdf` */

DROP TABLE IF EXISTS `pdf`;

CREATE TABLE `pdf` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `thumbnail` varchar(255) DEFAULT NULL,
  `category_id` int(10) unsigned NOT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `FK_file_upload_category` (`category_id`),
  KEY `FK_file_upload_user` (`username`),
  CONSTRAINT `FK_file_upload_user` FOREIGN KEY (`username`) REFERENCES `user` (`username`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_file_upload_category` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `pdf` */

LOCK TABLES `pdf` WRITE;

insert  into `pdf`(`id`,`username`,`file_name`,`display_name`,`thumbnail`,`category_id`,`created_time`) values (1,'test1','Lab1_WP10.pdf','file 10',NULL,1,'2015-04-04 23:29:38'),(2,'test1','Lab1_WP.pdf',NULL,NULL,1,'2015-04-04 23:33:23'),(3,'test123456','Lab1_WP.pdf','Testing',NULL,3,'2015-04-09 21:40:37');

UNLOCK TABLES;

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `gender` enum('male','female','unknow') NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`),
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `user` */

LOCK TABLES `user` WRITE;

insert  into `user`(`id`,`username`,`password`,`email`,`firstname`,`lastname`,`gender`,`create_time`) values (1,'nhatth','123456','','','','','2015-04-04 22:57:51'),(4,'test','123','','','','','2015-04-09 21:31:32'),(2,'test1','123','','','','','2015-04-04 22:57:51'),(5,'test123','123456','','','','','2015-04-09 21:34:04'),(6,'test123456','123','','Nhat','','','2015-04-09 21:39:54'),(3,'testing','123','tranhoangnhat5683@gmail.com','Nhat','Tran Hoang','male','2015-04-09 20:38:23');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
